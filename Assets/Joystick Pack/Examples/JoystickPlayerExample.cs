﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickPlayerExample : MonoBehaviour
{
    public float speed;
    VariableJoystick variableJoystick;
    Rigidbody rb;

    Vector3 direction;

    Animator animator;

    private void Start()
    {
        variableJoystick = FindObjectOfType<VariableJoystick>();
        rb = GetComponent<Rigidbody>();

        animator = GetComponent<Animator>();
    }

    public void FixedUpdate()
    {
        direction = new Vector3(variableJoystick.Horizontal * speed, 0, variableJoystick.Vertical * speed);
        rb.velocity = direction;

        if (rb.velocity != Vector3.zero)
        {
            transform.rotation = Quaternion.LookRotation(rb.velocity);
            animator.SetBool("Run", true);
        }
        else
            animator.SetBool("Run", false);

    }
}