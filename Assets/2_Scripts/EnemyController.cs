using System.Collections;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    /*public GameObject bullet;
    public GameObject bulletSpawnPointObj;

    public GameObject muzzleParticle;*/

    [Space(10)]
    public float detectionDistance;
    public float questionDistance;
    public float attackDistance;

    GameObject questionMark;
    GameObject exclamationMark;

    PlayerController PC;

    NavMeshAgent navMeshAgent;
    Rigidbody rig;
    Animator animator;

    Vector3 target;
    Vector3 destination;

    bool getHit;
    bool isPlayerVisible;
    bool questionMark_Bool;
    bool isAwared;
    bool isHit;
    bool walk = true;
    bool stop;

    Collider[] rigColliders;
    Rigidbody[] rigRigidbodies;

    RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        if (detectionDistance == 0)
            detectionDistance = 10;

        if (questionDistance == 0)
            questionDistance = 15;

        if (attackDistance == 0)
            attackDistance = 2;

        PC = FindObjectOfType<PlayerController>();

        navMeshAgent = GetComponent<NavMeshAgent>();
        rig = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        rigColliders = GetComponentsInChildren<Collider>();
        rigRigidbodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rigRigidbodies)
            rb.isKinematic = true;
        foreach (Collider col in rigColliders)
            if (col.tag != "Weapon")
                col.enabled = false;

        GetComponent<Collider>().enabled = true;
        rig.isKinematic = false;

        questionMark = transform.GetChild(1).GetChild(0).gameObject;
        exclamationMark = transform.GetChild(1).GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 thisV3 = new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z);
        Vector3 playerV3 = new Vector3(PC.transform.position.x, PC.transform.position.y + 0.8f, PC.transform.position.z);

        if (Physics.Linecast(thisV3, playerV3, out hit))
        {
            Debug.DrawLine(thisV3, playerV3, Color.red);

            if (hit.collider.tag == "Player")
            {
                if (Vector3.Distance(transform.position, PC.transform.position) < attackDistance && !getHit)
                {
                    questionMark.SetActive(false);
                    exclamationMark.SetActive(true);

                    navMeshAgent.isStopped = true;
                    isPlayerVisible = true;
                    animator.SetBool("Run", false);
                }
                else if (Vector3.Distance(transform.position, PC.transform.position) < detectionDistance && !getHit)
                {
                    exclamationMark.SetActive(true);
                    questionMark.SetActive(false);

                    navMeshAgent.isStopped = false;
                    isPlayerVisible = true;
                    animator.SetBool("Run", true);
                    target = PC.transform.position;
                    navMeshAgent.SetDestination(target);
                }
                else if (Vector3.Distance(transform.position, PC.transform.position) < questionDistance && !getHit)
                {
                    questionMark.SetActive(true);
                    exclamationMark.SetActive(false);
                }
            }
            else
            {
                navMeshAgent.isStopped = false;

                if (isPlayerVisible)
                {
                    target = PC.transform.position;
                    navMeshAgent.SetDestination(target);
                    animator.SetBool("Run", true);
                }
            }
        }

        Awared();

        if (questionMark_Bool && !exclamationMark.activeSelf && !stop)
        {
            questionMark.SetActive(true);
            exclamationMark.SetActive(false);

            stop = true;
            questionMark_Bool = false;
        }

        if (questionMark.activeSelf)
        {
            destination = Random.insideUnitSphere * 50;

            destination += transform.position;

            NavMeshHit hit;
            NavMesh.SamplePosition(destination, out hit, 50, 500);

            destination = new Vector3(destination.x, transform.position.y, destination.z);

            if (!getHit && !WalkFunc(destination))
            {
                navMeshAgent.isStopped = false;

                navMeshAgent.SetDestination(destination);
                animator.SetBool("Run", true);
            }
        }
    }

    void Awared()
    {
        float Mindiff = 4f;
        float diff;

        foreach (var item in FindObjectsOfType<EnemyController>())
        {
            if (item.transform.GetChild(1).GetChild(0).gameObject.activeSelf)
            {
                diff = Vector3.Distance(transform.position, item.transform.position);

                if (diff <= Mindiff)
                {
                    isAwared = true;
                }
            }
        }
    }

    bool WalkFunc(Vector3 nextPosition)
    {
        if (nextPosition == transform.position)
        {
            walk = true;
            return true;
        }
        else { return false; }
    }
}