using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    PlayerController PC;

    float xDif, zDif;

    Vector3 initialPos;

    // Start is called before the first frame update
    void Start()
    {
        PC = FindObjectOfType<PlayerController>();

        initialPos = transform.position;

        xDif = initialPos.x - PC.transform.position.x;
        zDif = initialPos.z - PC.transform.position.z;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(PC.transform.position.x + xDif, initialPos.y, PC.transform.position.z + zDif);
    }
}