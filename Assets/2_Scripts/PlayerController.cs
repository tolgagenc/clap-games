using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Finish" || other.tag == "Enemy")
        {
            SceneManager.LoadScene(0);
        }
    }
}