using System.Collections;
using System.Runtime.ConstrainedExecution;
using UnityEngine;
using UnityEngine.AI;

public class PoliceController : MonoBehaviour
{
    public enum EnemyType
    {
        Gun, Melee
    }
    public EnemyType enemyType;

    public GameObject bullet;
    public GameObject bulletSpawnPointObj;

    [Space(10)]
    public float detectionDistance;
    public float questionDistance;
    public float attackDistance;

    GameObject questionMark;
    GameObject exclamationMark;

    PlayerController PC;

    NavMeshAgent navMeshAgent;
    Rigidbody rig;
    Animator animator;

    Vector3 bulletSpawnPoint;
    Vector3 target;
    Vector3 destination;

    bool getHit;
    bool isPlayerVisible;
    bool questionMark_Bool;
    bool isAwared;
    bool isHit;
    bool walk = true;
    bool stop;

    Collider[] rigColliders;
    Rigidbody[] rigRigidbodies;

    RaycastHit hit;

    // Start is called before the first frame update
    void Start()
    {
        if (detectionDistance == 0)
            detectionDistance = 5;

        if (questionDistance == 0)
            questionDistance = 6;

        if (attackDistance == 0 && enemyType == EnemyType.Gun)
            attackDistance = 4;
        else if (attackDistance == 0 && enemyType == EnemyType.Melee)
            attackDistance = 1f;

        PC = FindObjectOfType<PlayerController>();

        navMeshAgent = GetComponent<NavMeshAgent>();
        rig = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();

        rigColliders = GetComponentsInChildren<Collider>();
        rigRigidbodies = GetComponentsInChildren<Rigidbody>();
        foreach (Rigidbody rb in rigRigidbodies)
            rb.isKinematic = true;
        foreach (Collider col in rigColliders)
            if (col.tag != "Weapon")
                col.enabled = false;

        GetComponent<Collider>().enabled = true;
        rig.isKinematic = false;

        questionMark = transform.GetChild(1).GetChild(0).gameObject;
        exclamationMark = transform.GetChild(1).GetChild(1).gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 thisV3 = new Vector3(transform.position.x, transform.position.y + 0.8f, transform.position.z);
        Vector3 playerV3 = new Vector3(PC.transform.position.x, PC.transform.position.y + 0.8f, PC.transform.position.z);

        if (Physics.Linecast(thisV3, playerV3, out hit))
        {
            Debug.DrawLine(thisV3, playerV3, Color.red);

            if (enemyType == EnemyType.Gun)     // Gun
            {
                if (hit.collider.tag == "Player")
                {
                    if (Vector3.Distance(transform.position, PC.transform.position) < detectionDistance && !getHit)
                    {
                        questionMark.SetActive(false);
                        exclamationMark.SetActive(true);

                        navMeshAgent.isStopped = true;
                        isPlayerVisible = true;
                        animator.SetBool("Fire", true);
                        animator.SetBool("Run", false);
                        target = PC.transform.position;
                        transform.LookAt(target);
                    }
                    
                    else if (Vector3.Distance(transform.position, PC.transform.position) < questionDistance && !getHit && isPlayerVisible || isAwared)
                    {
                        questionMark.SetActive(true);
                        exclamationMark.SetActive(false);
                    }
                }
                else if (!getHit && exclamationMark.activeSelf)
                {
                    navMeshAgent.isStopped = false;
                    animator.SetBool("Fire", false);

                    if (isPlayerVisible)
                    {
                        target = PC.transform.position;
                        navMeshAgent.SetDestination(target);
                        animator.SetBool("Run", true);
                    }
                }
            }
        }

        if (questionMark_Bool && !exclamationMark.activeSelf && !stop)
        {
            questionMark.SetActive(true);
            exclamationMark.SetActive(false);

            stop = true;
            questionMark_Bool = false;
        }

        if (questionMark.activeSelf)
        {
            destination = Random.insideUnitSphere * 100;

            destination += transform.position;

            NavMeshHit hit;
            NavMesh.SamplePosition(destination, out hit, 100, 1000);

            destination = new Vector3(destination.x, transform.position.y, destination.z);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "FiredStaff" && !getHit && tag != "Death")
        {
            transform.GetChild(0).GetComponent<ParticleSystem>().Play();
            transform.GetChild(0).SetParent(null);
            OnAIDeath();
        }

        else if (collision.gameObject.tag != "Floor")
        {
            isHit = true;
            walk = false;
        }
    }

    public void OnAIDeath()
    {
        animator.SetBool("Walk", false);
        animator.SetBool("Fire", false);
        animator.enabled = false;
        getHit = true;
        isAwared = false;
        navMeshAgent.enabled = false;

        tag = "Death";

        transform.GetChild(0).gameObject.SetActive(false);

        GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        StartCoroutine(StopRagdoll());
    }

    IEnumerator StopRagdoll()
    {
        foreach (Rigidbody rb in rigRigidbodies)
            rb.isKinematic = false;
        foreach (Collider col in rigColliders)
            col.enabled = true;

        GetComponent<Collider>().enabled = false;

        yield return new WaitForSeconds(0);
    }

    public void SendBullet()
    {
        GameObject newBullet = Instantiate(bullet);
        bulletSpawnPoint = bulletSpawnPointObj.transform.position;

        newBullet.transform.position = bulletSpawnPoint;
        Vector3 rotation = bullet.transform.rotation.eulerAngles;

        newBullet.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);
        newBullet.GetComponent<Rigidbody>().AddForce(bulletSpawnPointObj.transform.forward * 10, ForceMode.Impulse);
    }

    bool WalkFunc(Vector3 nextPosition)
    {
        if (nextPosition == transform.position)
        {
            walk = true;
            return true;
        }
        else { return false; }
    }
}
